
## To apply a 3dLUT (.cube file)

Use the option -filter:v lut3d='mylut.cube' (Need to be a cubic lut file).

For example to apply the file mylut.cub on a video

```bash
ffmpeg -i INPUT_FILE -filter:v lut3d='mylut.cube' OUTPUT_FILE
```

The filters can be created in the GIMP with G'MIC-QT plugin called *CLUT from After - Before Layers*.

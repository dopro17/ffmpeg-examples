# Video speed UP and slow DOWN

## Speed UP

This command will speed up the video by TIMES with output file with 60fps, use the fps filter parameter to avoid an output file with no or black frames.

```bash
ffmpeg -i IMPPUT_FILE -filter:v "setpts=PTS/TIMES,fps=60'" OUTPUT_FILE
```

For example to speed up by 5 times.

```bash
ffmpeg -i IMPPUT_FILE -filter:v "setpts=PTS/5,fps=60'" OUTPUT_FILE
```

## Slown Down

Same as speed up but with PTS*TIMES.

For example, to slow down by 2 times.

```bash
ffmpeg -i IMPPUT_FILE -filter:v "setpts=PTS*2,fps=60'" OUTPUT_FILE
```

